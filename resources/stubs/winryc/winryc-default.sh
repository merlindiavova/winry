# shellcheck disable=SC2148,2034

# Define environment variables

WINRYC_CONFIG_TYPE=__projectconfigtype__
APP_DIR=app
CONTEXT_DIRS='app'
DOCKER_WORKING_DIRECTORY=__workingdirectory__
PHP_COMPOSER_PKGS=__phpglobalcomposerpackages__

TEST_PIPELINE='phpspec:app|run phpunit:app'
ANALYSE_PIPELINE="phpstan:app|analyse psalm:app"
CI_PIPELINE="${ANALYSE_PIPELINE} ${TEST_PIPELINE} phpcs:app"

# Hook functions
# Uncomment and modify the required functions.

# winryc_extra_init() {
#     println 'info' 'This is from the extra init function'
# }

# winryc_context_extra_init() {
#     println 'info' 'This is from the extra context init function'
# }

# winryc_hook_pre_cleanup() {
#     println 'info' 'This is from the pre cleanup hook function'
# }

# winryc_hook_post_cleanup() {
#     println 'info' 'This is from the post cleanup hook function'
# }

# winryc_extra_commands() {
#     while [ "${1+defined}" ]; do
#         case "$1" in
#             extra) println 'A New command, added';
#         esac
#         shift $(($# ? 1 : 0))
#     done
# }