#!/usr/bin/env sh
# shellcheck disable=SC1090

set -eu

# Name:        winry foundation
# Version:     0.3.0
# Description: Defines functions and variables. Should be sourced by other scripts.
#
# To the extent possible under law, Merlin Diavova <merlindiavova@pm.me>
# has waived all copyright and related or neighboring rights to this work.
# http://creativecommons.org/publicdomain/zero/1.0/

[ -n "${TRACE+defined}" ] && set -x

WINRY_CURRENT_STEP=0
WINRY_HELP_TAB_LENGTH=10
WINRY_SCRIPT_VERSION=${WINRY_SCRIPT_VERSION:=0.6.0}
readonly WINRY_SCRIPT_NAME="$(basename "$0")"
WINRY_CONFIG_FILE="${WINRY_BASE_PATH}/.env"

WINRY_SED_COMMAND='sed -i'

case "$(uname -s)" in
    'Darwin') WINRY_SED_COMMAND='sed -i .bak' ;;
esac

# IO 20 - 39
WINRY_E_WRITE_FAILED=20
WINRY_E_COPY_FAILED=21
WINRY_E_MOVE_FAILED=22

# Exists/Found 40 - 59
WINRY_E_NOT_FOUND=40
WINRY_E_ENVIRONMENT_NOT_FOUND=41
WINRY_E_COMMAND_NOT_FOUND=127
WINRY_E_OPTION_NOT_FOUND=128
WINRY_E_PROGRAM_NOT_INSTALLED=42
WINRY_E_ALREADY_EXISTS=43

# Arguments 50 -79
WINRY_E_INVALID_ARGUMENTS_COUNT=60
WINRY_E_INVALID_ARGUMENTS=61
WINRY_E_UNDEFINED_VALUE=62
WINRY_E_VALUE_ASSIGNMENT_FAILED=63

# Permission
WINRY_E_CHANGE_PERMISSION_FAILED=80

# System 100 - 120
WINRY_E_MACHINE_UNSUPPORTED=100
WINRY_E_ENVIRONMENT_NOT_RUNNING=101

# Shell builtin errors
WINRY_CANNOT_EXECUTE_PROGRAM=126

export readonly WINRY_E_INVALID_ARGUMENTS_COUNT WINRY_E_INVALID_ARGUMENTS
export readonly WINRY_E_NOT_FOUND WINRY_E_COMMAND_NOT_FOUND WINRY_E_MOVE_FAILED
export readonly WINRY_E_UNDEFINED_VALUE WINRY_E_ALREADY_EXISTS
export readonly WINRY_E_VALUE_ASSIGNMENT_FAILED WINRY_E_MACHINE_UNSUPPORTED
export readonly WINRY_E_CHANGE_PERMISSION_FAILED WINRY_E_WRITE_FAILED
export readonly WINRY_E_ENVIRONMENT_NOT_FOUND WINRY_E_ENVIRONMENT_NOT_RUNNING
export readonly WINRY_E_PROGRAM_NOT_INSTALLED WINRY_E_COPY_FAILED
export readonly WINRY_CANNOT_EXECUTE_PROGRAM WINRY_E_OPTION_NOT_FOUND

export WINRY_BASE_PATH WINRY_VERSION WINRY_SCRIPT_NAME WINRY_CURRENT_STEP
export WINRY_SED_COMMAND WINRY_HELP_TAB_LENGTH WINRY_CONFIG_FILE

WINRY_COPY_COMMAND=${WINRY_COPY_COMMAND:='cp'}
command -v rsync >/dev/null 2>&1 && WINRY_COPY_COMMAND='rsync -aH'
export WINRY_COPY_COMMAND

trap 'die "Interrupted! exiting..."' INT TERM HUP

die() {
    [ -n "${WINRY_QUIET:-}" ] && unset WINRY_QUIET
    println red "[FATAL] $*"

    case "$(type cleanup 2>/dev/null)" in
        *function) cleanup
    esac

    exit 1
}

print_line() {
	line_break='\n'

	colour_code="${2:-\033[0;37m}";
	command printf "${colour_code}%s\033[m${line_break}" "${1}" 2>/dev/null
	unset line_break colour_code
}

println() {
    case "${1}" in
        blue) shift $(($# ? 1 : 0)); print_line "$@" '\033[0;34m';;
        bold) shift $(($# ? 1 : 0)); print_line "$@" '\033[1;37m';;
        bold_yellow) shift $(($# ? 1 : 0)); print_line "$@" '\033[0;33m' ;;
        green) shift $(($# ? 1 : 0)); print_line "$@" '\033[0;32m' ;;
        yellow) shift $(($# ? 1 : 0)); print_line "$@" '\033[0;93m' ;;
        red) shift $(($# ? 1 : 0)); print_line "$@" '\033[0;91m' ;;

        error) shift $(($# ? 1 : 0)); print_line "[error]    $*" '\033[0;91m'; exit 1 ;;
        info)
            if [ -n "${WINRY_VERBOSE:-}" ]; then
                shift $(($# ? 1 : 0))
                print_line "[info]     $*" '\033[0;96m'
            fi
        ;;
        done) shift $(($# ? 1 : 0)); print_line "[done]     $*" '\033[0;96m' ;;

        section) command printf '\n\n' -- ;;
        space) command printf '\n' -- ;;
        step)
            shift $(($# ? 1 : 0))
            WINRY_CURRENT_STEP=$((WINRY_CURRENT_STEP+1))
            command printf \
                '[%d/%d]      %s\n' \
                "${WINRY_CURRENT_STEP}" \
                "${WINRY_STEP_COUNT}" \
                "$*"
        ;;
        *) command printf '%s\n' "$*" 2>/dev/null ;;
    esac
}

print_error() {
    [ -n "${WINRY_QUIET:-}" ] && unset WINRY_QUIET

    extra_message=''
    error_code="$1"; shift $(($# ? 1 : 0));

    [ -n "$*" ] && extra_message="${*}. "

    case "${error_code}" in
        # WINRY_E_INVALID_ARGUMENTS_COUNT
        60) error_message='Invalid number of arguments given.' ;;
        # WINRY_E_INVALID_ARGUMENTS
        61) error_message='Invalid arguments supplied' ;;
        # WINRY_E_NOT_FOUND
        40) error_message='Not found.' ;;
        # WINRY_E_ENVIRONMENT_NOT_FOUND
        41) error_message='Environment Not found.' ;;
        # WINRY_E_COMMAND_NOT_FOUND
        127) error_message='Unknown command' ;;
        # WINRY_E_OPTION_NOT_FOUND
        128) error_message='Option not found' ;;
        # WINRY_E_PROGRAM_NOT_INSTALLED
        42) error_message='Program not installed' ;;
        # WINRY_E_UNDEFINED_VALUE
        13) error_message='Undefined value.' ;;
        # WINRY_E_CONTAINER_NOT_SET
        14) error_message='Conainer is not' ;;
        # WINRY_E_ALREADY_EXISTS
        43) error_message='Already exists.' ;;
        # WINRY_E_WRITE_FAILED
        15) error_message='Failed to write resource.' ;;
        # WINRY_E_COPY_FAILED
        21) error_message='Failed to copy file/folder.' ;;
        # WINRY_E_MOVE_FAILED
        22) error_message='Failed to move file/folder.' ;;
        # WINRY_E_VALUE_ASSIGNMENT_FAILED
        25) error_message='Failed to assign a value.' ;;
        # WINRY_E_CHANGE_PERMISSION_FAILED
        30) error_message='chmod operation failed.' ;;
        # WINRY_E_MACHINE_UNSUPPORTED
        99)
        error_message='Only, Linux, MacOS and Windows systems are supported.'
        ;;
        # WINRY_E_ENVIRONMENT_NOT_RUNNING
        100) error_message='Environment is not running' ;;

        # Unknown
        *) error_message='Unknown error code'
    esac

    println error "${error_message} - ${extra_message}" >&2
    exit "$error_code"
}

usage_indentation() {
	num_of_spaces=${1:-2}

	command printf "%${num_of_spaces}s"
	unset num_of_spaces
}

usage_it_gap() {
	subject="$*"
	subject_length="${#subject}"
	spaces="$((WINRY_HELP_TAB_LENGTH - subject_length))"

	if [ 0 -ge "$spaces" ]; then
		command printf "%d" 0
	else
		command printf "%d" "$spaces"
	fi

	unset subject subject_length spaces
}

print_usage_header() {
	command printf ''
	command printf '\033[1;37m%s\033[m\n\n' "$1"
	command printf ''
	command printf '\033[0;33mUsage:\033[m\n  %s\n\n' "$2"
}

print_usage_line() {
	usage_line_item="$(println green "$1")"
	usage_line_item_description="$2"
	usage_line_spaces="$(usage_it_gap "$1")"
	usage_line_total_spaces="$((usage_line_spaces + 1))"
	indentation="$(usage_indentation $usage_line_total_spaces)"

	command printf \
        "  %s${indentation}%s\n" \
        "$usage_line_item" \
        "$usage_line_item_description"

	unset usage_line_item usage_line_item_description
	unset usage_line_spaces usage_line_total_spaces indentation
}

print_command_usage_line() {
	item="$(println green "$1")"
	item_usage="$2"
	item_description="$3"
	spaces="$(usage_it_gap "$1 $2")"
	total_spaces="$((spaces + 1))"

	indentation="$(usage_indentation "$total_spaces")"

	command printf \
        "  %s %s${indentation}%s\n" \
        "$item" "$item_usage" \
        "$item_description"

	unset item item_usage item_description spaces total_spaces indentation
}

print_version() {
    [ "$WINRY_SCRIPT_NAME" = 'winry' ] || prefix="winry-"
	display_version="$(println yellow "${WINRY_SCRIPT_VERSION}")"

	command printf '\n'
	command printf "🔧 %s %s" "${prefix:-}$WINRY_SCRIPT_NAME" "$display_version"
	command printf '\n'
	unset display_version
}

export_env_variables() {
	[ ! -r "${1}" ] && die "Cannot read ${1}"
	set -o allexport
	# shellcheck disable=SC1090,SC1091
	\. "${1}"
	set +o allexport
}

parse_base_arguments() {
    [ "$WINRY_SCRIPT_NAME" = 'winry' ] || prefix="winry-"
	while getopts "cqhvV" opt; do
		case $opt in
			c) shift $(($# ? 1 : 0)); export WINRY_CONFIG_FILE="$1" ;;
			h) print_usage; exit 0 ;;
			v) export WINRY_VERBOSE=1 ;;
			V) println "${prefix:-}$WINRY_SCRIPT_NAME $WINRY_SCRIPT_VERSION"; exit 0 ;;
			q) export WINRY_QUIET=1 ;;
			*) print_error "Unknown option [$1]"; exit 1 ;;
		esac
		shift $(($# ? 1 : 0))
	done
}

assert_required_variables() {
	return 0
}

bootstrap() {
    export_env_variables "${WINRY_CONFIG_FILE}"
    assert_required_variables
}

# shellcheck disable=SC2086,SC2048
trim_all() {
    # Usage: trim_all "   example   string    "

    # Disable globbing to make the word-splitting below safe.
    set -f

    # Set the argument list to the word-splitted string.
    # This removes all leading/trailing white-space and reduces
    # all instances of multiple spaces to a single ("  " -> " ").
    set -- $*

    # Print the argument list as a string.
    printf '%s\n' "$*"

    # Re-enable globbing.
    set +f
}

left_strip() {
	command printf '%s\n' "${1##$2}"
}

right_strip() {
	command printf '%s\n' "${1%%$2}"
}

copy() {
    $WINRY_COPY_COMMAND "$1" "$2" || print_error "${WINRY_E_COPY_FAILED}" "$1 to [$2]"
}

head() {
    while IFS= read -r line; do
        printf '%s\n' "$line"
        i=$((i+1))
        [ "$i" = "$1" ] && return
    done < "$2"

    [ -n "$line" ] && printf %s "$line"
}

title_case_word() {
    without_first_letter="${1#?}"
    string="$(
        command printf '%s' "${1%%$without_first_letter}" \
        | tr '[:lower:]' '[:upper:]'
    )${without_first_letter}"

    command printf '%s\n' "$string"
}

uppercase() {
    command printf '%s' "${1}" | tr '[:lower:]' '[:upper:]'
}

basename() {
    # Usage: basename "path" ["suffix"]

    # Strip all trailing forward-slashes '/' from
    # the end of the string.
    #
    # "${1##*[!/]}": Remove all non-forward-slashes
    # from the start of the string, leaving us with only
    # the trailing slashes.
    # "${1%%"${}"}:  Remove the result of the above
    # substitution (a string of forward slashes) from the
    # end of the original string.
    dir=${1%${1##*[!/]}}

    # Remove everything before the final forward-slash '/'.
    dir=${dir##*/}

    # If a suffix was passed to the function, remove it from
    # the end of the resulting string.
    dir=${dir%"${2:-}"}

    # Print the resulting string and if it is empty,
    # print '/'.
    printf '%s\n' "${dir:-/}"
}